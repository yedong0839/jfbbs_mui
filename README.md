用MUI结合[JFinal社区](http://jfbbs.tomoya.cn)开发的JFinal社区客户端

给大家截图分享一下

侧滑：

![](http://static.tomoya.cn/E8E7C9312731633FE60DAAB1256F0FB8.png)

列表：

![](http://static.tomoya.cn/1C15BCD95A896BE54FD262BEA57122C1.png)

详情：

![](http://static.tomoya.cn/131C9F983A25C272FA60DB659D94B56E.png)

回复列表：

![](http://static.tomoya.cn/520EDEEE93D3233C4B2BCE13FE7A6D0E.png)

创建话题：

![](http://static.tomoya.cn/2CB3DC9807FEB0274DA439DD8005BA03.png)

我的：

![](http://static.tomoya.cn/91228AE1376F288A6D8D8FF532F37494.png)

我的消息：

![](http://static.tomoya.cn/A45B797A9929D1BCF5BA2C662575A42A.png)

关于：

![](http://static.tomoya.cn/1BA4FD0807A7BCF4F764DE1388320509.png)

#### MUI版本

Android下载地址：[JFinal社区-v1.0.0.apk](http://static.tomoya.cn/JFinal社区-v1.0.0.apk)

ios越狱下载地址：[JFinal社区-v1.0.0.ipa](http://static.tomoya.cn/JFinal社区-v1.0.0.ipa)

#### 原生版本

Android下载地址：[JF社区v1.0.1.apk](http://pan.baidu.com/s/1jGjE7t0?qq-pf-to=pcqq.group)